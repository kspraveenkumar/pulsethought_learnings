package com.pulsethought.starter.toggle;

import com.pulsethought.starter.utils.Logger;
import org.togglz.core.repository.FeatureState;
import org.togglz.core.repository.StateRepository;
import org.togglz.core.repository.cache.CachingStateRepository;

import java.util.concurrent.TimeUnit;


public class CustomFeatureToggleRepository extends CachingStateRepository {

    private Logger logger = Logger.getInstance(CustomFeatureToggleRepository.class);

    public CustomFeatureToggleRepository(StateRepository stateRepository, long i, TimeUnit unit) {
        super(stateRepository, i, unit);
    }

    @Override
    public void setFeatureState(FeatureState featureState) {
        super.setFeatureState(featureState);
        logger.info(Logger.Category.ADMIN_ACTIVITY, featureState.getFeature().name() + " is " + featureState.isEnabled());
    }
}
