package com.pulsethought.starter.toggle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.togglz.core.manager.EnumBasedFeatureProvider;
import org.togglz.core.repository.StateRepository;
import org.togglz.core.repository.cache.CachingStateRepository;
import org.togglz.core.repository.jdbc.JDBCStateRepository;
import org.togglz.core.repository.util.DefaultMapSerializer;
import org.togglz.core.spi.FeatureProvider;

import javax.sql.DataSource;
import java.util.concurrent.TimeUnit;

@Configuration
public class ToggleConfig {

    @Autowired
    private DataSource dataSource;

    @Bean
    public FeatureProvider featureProvider() {
        return new EnumBasedFeatureProvider(Features.class);
    }

    @Bean
    public StateRepository stateRepository() {
        JDBCStateRepository jdbcStateRepository = JDBCStateRepository.newBuilder(dataSource)
                .tableName("features")
                .createTable(false)
                .serializer(DefaultMapSerializer.singleline())
                .noCommit(true)
                .build();
        return new CustomFeatureToggleRepository(new CachingStateRepository(jdbcStateRepository), 5, TimeUnit.SECONDS);
    }
}
