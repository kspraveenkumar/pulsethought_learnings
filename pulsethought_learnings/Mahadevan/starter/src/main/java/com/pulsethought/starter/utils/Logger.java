package com.pulsethought.starter.utils;

import org.slf4j.LoggerFactory;

public class Logger {

    private org.slf4j.Logger slf4jLogger;

    private Logger(Class<?> clazz) {
        slf4jLogger = LoggerFactory.getLogger(clazz);
    }

    public static Logger getInstance(Class<?> clazz) {
        return new Logger(clazz);
    }

    public void info(Category category, String msg) {
        slf4jLogger.info(category.getValue() + " " + msg);
    }

    public void info(Category category, String format, Object... arguments) {
        slf4jLogger.info(category.getValue() + " " + format, arguments);
    }

    public void info(Category category, String msg, Throwable t) {
        slf4jLogger.info(category.getValue() + " " + msg, t);
    }

    public void error(Category category, String msg) {
        slf4jLogger.error(category.getValue() + " " + msg);
    }

    public void error(Category category, String format, Object... arguments) {
        slf4jLogger.error(category.getValue() + " " + format, arguments);
    }

    public void error(Category category, String msg, Throwable t) {
        slf4jLogger.error(category.getValue() + " " + msg, t);
    }


    public enum Category {
        ADMIN_ACTIVITY("[Admin Activity]"),
        EMPTY("");

        private String category;

        Category(String category) {
            this.category = category;
        }

        public String getValue() {
            return category;
        }
    }
}
