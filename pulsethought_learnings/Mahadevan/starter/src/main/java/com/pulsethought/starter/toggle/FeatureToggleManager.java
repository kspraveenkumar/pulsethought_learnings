package com.pulsethought.starter.toggle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.togglz.core.manager.FeatureManager;

@Component
public class FeatureToggleManager {
    @Autowired
    private FeatureManager manager;

    public boolean isActive(Features feature) {
        return manager.isActive(feature);
    }
}
